from django.contrib import admin
from .models import Personas, Productos, Categorias

admin.register(Personas)
admin.register(Productos)
admin.register(Categorias)