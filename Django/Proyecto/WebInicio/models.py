from django.db import models

class Personas(models.Model):
    ID = models.IntegerField(primary_key=True)
    NOMBRE = models.TextField(max_length=30)
    APELLIDO = models.TextField(max_length=30)
    TELEFONO = models.TextField(max_length=30)
    EMAIL = models.EmailField(max_length=30)
    USUARIO = models.TextField(max_length=30)
    CLAVE = models.TextField(max_length=30)

class Productos(models.Model):
    ID = models.IntegerField(primary_key=True)
    NOMBRE_PRODUCTO = models.TextField(max_length=30)
    PRECIO = models.DecimalField(max_digits=10, decimal_places=3)
    DESCRIPCION = models.TextField(max_length=30)

class Categorias(models.Model):
    ID = models.IntegerField(primary_key=True)
    NOMBRE_CATEGORIA = models.TextField(max_length=30)
