from django.shortcuts import render, redirect
from .models import Personas

def paginaInicio(request):
    return render(request, 'inicio.html')

def paginaRegistro(request):
    error = ""

    if request.method == 'POST':
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        telefono = request.POST['telefono']
        email = request.POST['email']
        usuario = request.POST['usuario']
        clave = request.POST['clave']
        if Personas.objects.filter(EMAIL=email).exists():
            error = "*El usuario ya está registrado."
        elif Personas.objects.filter(USUARIO=usuario).exists():
            error = "*Este nombre de usuario ya está ocupado."
        else:
            nuevoDato = Personas.objects.create(
                NOMBRE=nombre,
                APELLIDO=apellido,
                TELEFONO=telefono,
                EMAIL=email,
                USUARIO=usuario,
                CLAVE=clave
            )
            return redirect('/Login/')

    return render(request, 'registro.html', {"error": error})

def paginaLogin(request):
    error = ""
    if request.method == 'POST':
        try:
            usuario = request.POST['usuario']
            clave = request.POST['clave']
            verificarUsuario = Personas.objects.get(USUARIO=usuario)
            if verificarUsuario.CLAVE == clave:
                return redirect('/Marketplace/')
            else:
                error = "*La contraseña ingresada es incorrecta."
        except:
            error = "*El usuario no está registrado."
            
    return render(request, 'login.html', {
        "error":error
    })

def paginaMarketplace(request):
    return render(request, 'marketplace.html')

def paginaCategoria(request):
    return render(request, 'categoria.html')

def paginaCategoria2(request):
    return render(request, 'categoria2.html')

def paginaProducto(request, parametro1):
    productos = [
        {"nombre": "Nitro tech", "descripcion": "Es pura y simple, y cumple con sus promesas. Una sola cucharada medidora de Nitro-Tech® contiene 30 g de proteína de suero de leche de primera calidad que aporta aminoácidos esenciales y aminoácidos de cadena ramificada. Se filtra con la tecnología de filtración multifase para eliminar las grasas y los carbohidratos no deseados. Después de todo, el contenido es lo que cuenta.", "precio": 200.000},
        {"nombre": "ISO100", "descripcion": "ISO 100 de 1.4 lbs de vainilla gourmet de la marca Dymatize es un suplemento de proteína de suero de alta calidad diseñado para satisfacer las necesidades de los atletas y entusiastas del fitness. Cada porción de 25 gramos de este producto ofrece 20 servicios en total y proporciona 25 gramos de proteína. ISO 100 de Dymatize utiliza una fórmula de proteína de suero aislada que ha sido procesada para eliminar la mayoría de los carbohidratos y las grasas, brindando una proteína de rápida absorción y altamente biodisponible.Esto significa que tu cuerpo puede utilizar eficientemente los aminoácidos esenciales presentes en el suero de leche para la síntesis de proteínas y la recuperación muscular.", "precio": 380.000},
        {"nombre": "Gold standard whey", "descripcion": "En más de 90 países, durante más de 30 años, se ha confiado en Optimum Nutrition® para obtener la más alta calidad en recuperación posentrenamiento, energía preentrenamiento y nutrición deportiva para llevar. Después de una cuidadosa selección de proveedores, cada ingrediente se prueba para asegurar una pureza, potencia y composición excepcionales. Cumplimos con los más altos estándares de producción para que pueda liberar todo el potencial del cuerpo.", "precio": 250.000},
        {"nombre": "BIPRO", "descripcion": "BiPro es una fórmula nutricional elaborada a base de proteína de suero y caseína, proteínas lácteas con excelentes perfiles nutricionales y diferentes velocidades de absorción, contiene en su fórmula L-Carnitina y Picolinato de cromo, en las proporciones adecuadas. BiPro contiene avena como fuente de carbohidratos, ideal para personas que buscan alimentos con fibra y bajo índice glucémico. Además posee el más delicioso y cremoso sabor para disfrutar al máximo el programa nutricional.", "precio": 215.000},
        {"nombre": "Whey", "descripcion": "Whey Pure de 5 libras es un suplemento de proteína en polvo de alta calidad, formulado con aislado de proteína de suero de leche. Cada envase contiene 30 porciones, con 26 gramos de proteína por porción. El aislado de proteína de suero de leche es una de las formas más puras de proteína de suero de leche, lo que significa que contiene una mayor proporción de proteína y menos grasa y lactosa en comparación con otros tipos de proteína de suero de leche. Esto lo hace ideal para aquellos que buscan aumentar su ingesta de proteínas mientras mantienen bajo el consumo de carbohidratos y grasas.", "precio": 280.000},
        {"nombre": "MECA CARBS", "descripcion": "MEGA CARBS contiene 10g de Isomaltulosa, un tipo de carbohidrato que proporciona energía de lenta liberación y larga duración, MEGA CARBS contiene L-Glutamina, el aminóacido mas abundante en la masa muscular y 4g de BCAAS, aminoácidos que se catabolizan (convierten en energía) en programas de largo entrenamiento físico. MEGA CARBS contiene Trigliceridos de Cadena Media (TCM), una de las fuentes de energía más apetecidas por los músculos, ya que son utilizadas rápidamente como combustible.", "precio": 180.000},
        {"nombre": "Legacy", "descripcion": "Es nuestra creatina HCL de mejor y mas rapida absorcion, por lo que siempre vas a requerir menos cantidad para obtener los mismos resultados; es un producto que reune tres reconocidas sustancias en el campo del deporte y el fitness: arginina, metionina y glicinaque tienen una gran biodisponibilidad y excelente absorcion y por ultimo el metabolito final de la leucina que garantiza la sintesis proteica y estilula el desarrolo de la masa muscular.", "precio": 110.000},
        {"nombre": "Creatine", "descripcion": "Una fórmula de construcción muscular de gran alcance que ofrece una efectiva dosis de 5 gramos de monohidrato de creatina de alta calidad, una de las más eficaces y validadas científicamente en el mundo para mejorar tu rendimiento. La creatina proporciona energía a las células musculares y promueve aumentos en fuerza y masa muscular.", "precio": 95.000},
        {"nombre": "CREASTACK", "descripcion": "Crea Stack es un producto diseñado para incrementar la fuerza, la resistencia y la energía en actividades físicas. Su fórmula contiene creatina monohidratada (la creatina con más evidencia científica), HMB (estimula la síntesis de proteínas musculares), ácido alfa lipoico (incrementa el transporte de energía a los músculos) y sulfato de vanadio (potencializa los efectos anabólicos de la insulina). El efecto combinado de sus ingredientes activan la energía muscular y estimulan la síntesis de proteínas musculares.", "precio": 95.000},
        {"nombre": "CREATINE POWER", "descripcion": "Proteína suplementaria en apoyo de la masa muscular y la salud: Advantage Whey, Efectos de la suplementación de creatina de 4 semanas combinada con entrenamiento complejo sobre el daño muscular y el rendimiento deportivo, Efectos de la suplementación con creatina y tres días de entrenamiento de resistencia sobre la fuerza muscular, la producción de energía y la función neuromuscular", "precio": 125.000},
        {"nombre": "MICRONIZED", "descripcion": "La creatina se utiliza en todo tipo de deportes, no obstante y de acuerdo a su función, es más comúnmente ingerida en aquellos deportistas que realizan actividades donde su duración es muy corta (no mayor a 15 segundos), es decir, movimientos o actividades deportivas de explosión, velocidad y fuerza. La creatina incrementa la velocidad de resíntesis de ATP (molécula que da energía), siendo ésta su principal función entre muchas otras, además, la creatina monohidrato es de los suplementos con mayor evidencia científica en el mundo.", "precio": 150.000},
        {"nombre": "PLATINUM", "descripcion": "La creatina tiene la capacidad de incrementar los niveles de fosfocreatina en el interior de las células. Mayor cantidad de fosfocreatina significa más energía disponible para la actividad física de gran intensisdad. Se ha demostrado científicamente que una ingesta diaria de 5g de creatina mejora el rendimiento físico, en series sucesivas de ejercicios breves de alta intensidad, como el entrenamiento con pesas o el cardiovascular con intervalos. Por ser micronizada presenta la mayor solubilidad y absorción, aprovechando al máximo cada gramo de producto.", "precio": 120.000}
    ]

    producto_seleccionado = None
    for producto in productos:
        if producto["nombre"] == parametro1:
            producto_seleccionado = producto
            break

    

    return render(request, "producto.html", {"producto": producto_seleccionado})