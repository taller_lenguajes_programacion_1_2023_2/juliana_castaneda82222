#1er entregable, Juliana Castañeda
#Punto 8, determinar si una variable s es de tipo string o no
print("\nPunto 8")
print("Ingrese la variable: ")
variable =input()
print(type(variable))

#Punto 18, tomar una cadena s y devolverla invertida
print("\nPunto 18")
cadena_original = "Hola, mundo!"
cadena_invertida = cadena_original[::-1]

print(cadena_invertida)

#Punto 28, tomar dos listas y devuelva True si tienen al menos un miembro en común
print("\nPunto 28")
lista1 = [1, 2, 3, 4, 5]
lista2 = [5, 6, 7, 8, 9]

miembro_comun = False

for num1 in lista1:
    for num2 in lista2:
        if num1 == num2:
            miembro_comun = True
            break  
    if miembro_comun:
        break 
print(miembro_comun)

#Punto 38, sumar todos los valores de un diccionario
print("\nPunto 38")
Diccionario={
     "a": 20,
     "b": 30,
     "c": 10,
     "d": 40}

suma = 0
for valor in Diccionario.values():
    suma+=valor
print("La suma es: ", suma)

#Punto 48, combinar dos archivos CSV en uno solo
print("\nPunto 48")
import pandas as pd

df1 = pd.read_csv("src/trp1_sesiones/Documentos/csv/Clase7v1.csv")
df2 = pd.read_csv("src/trp1_sesiones/Documentos/csv/Clase7v3.csv")

Combinar = pd.concat([df1, df2], ignore_index=True)

Combinar.to_csv("src/trp1_sesiones/Documentos/csv/combinado.csv", index=False)

print("Guardado en 'combinado.csv'")

#Punto 58, convertir un archivo CSV en un archivo JSON
print("\nPunto 58")
df = pd.read_csv("src/trp1_sesiones/Documentos/csv/Clase6.csv")
json_archivo = df.to_json("src/trp1_sesiones/Documentos/json/datos.json")

print("Guardado en 'datos.json'")

#Punto 68, simula un archivo Excel alumnos.xlsx, ordena los datos por el promedio del alumno
print("\nPunto 68")
data = {
    'Nombre': ['Juan', 'María', 'Carlos', 'Laura'],
    'Edad': [20, 22, 21, 19],
    'Promedio': [4.1, 5.0, 3.8, 3.9]
}

df = pd.DataFrame(data)

df.to_excel("src/trp1_sesiones/Documentos/xlsx/alumnos.xlsx", index=False)
df_ordenado = pd.read_excel("src/trp1_sesiones/Documentos/xlsx/alumnos.xlsx")
df_ordenado = df_ordenado.sort_values(by='Promedio', ascending=False)

df_ordenado.to_excel("src/trp1_sesiones/Documentos/xlsx/alumnos.xlsx", index=False)

print("Guardado en 'alumnos.xlsx'")

#Punto 78, simula un archivo Excel, muestre un resumen estadístico de una columna numérica
print("\nPunto 78")
df = pd.read_excel("src/trp1_sesiones/Documentos/xlsx/clase_4_dataset.xlsx")

promedio = df['Edad'].mean()

print(f"Resumen estadístico: {promedio}")

#Punto 88, leer un archivo HTML y convierta una columna de precios de string con símbolo de moneda a float
print("\nPunto 88")
url = "https://www.capitalcolombia.com/sec-trm_precio_dolar_en_colombia"

tablas = pd.read_html(url)
df = tablas[0]
df['TRM (Precio en Pesos Colombianos)'] = df['TRM (Precio en Pesos Colombianos)'].str.replace('[\$,]', '', regex=True).astype(float)

print(df)


#Punto 98, simula un archivo Excel con una columna de texto, utilice expresiones regulares para extraer y contar palabras específicas
print("\nPunto 98")
df = pd.read_excel("src/trp1_sesiones/Documentos/xlsx/clase_4_dataset.xlsx")

texto = df["Nombre"]

ocurrencias = 0
for palabra in texto:
    if "Marta" in palabra:
        ocurrencias += 1

print("Marta está:", ocurrencias,"veces")