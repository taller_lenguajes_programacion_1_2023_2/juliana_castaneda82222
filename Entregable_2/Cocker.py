from Perro import Perro
import pandas as pd
from Conexion import Conexion
from Excel import Excel

class Cocker(Perro, Conexion):
    def __init__(self, raza="", color="", pelaje="", personalidad="", tamanio="", cola="", nombre="", largoOrejas="", nombreCriador="", nivelEnergia="", historiaSalud="", tipoPelaje=""):
        super().__init__(raza, color, pelaje, personalidad, tamanio, cola)
        """
        Atributos(Todos son de tipo String):
        Nombre: Qué nombre tiene el cocker
        LargoOrejas: Longitud de las orejas
        NombreCriador: Qué nombre tiene el dueño
        NivelEnergia: Qué nivel de energía tiene(alto, medio, bajo)
        HistoriaSalud: La historia de salud del animal
        TipoPelaje: Qué pelaje tiene(liso, ondulado, rizado, crespo)
        """
        self.nombre=nombre
        self.largoOrejas=largoOrejas
        self.nombreCriador=nombreCriador
        self.nivelEnergia=nivelEnergia
        self.historiaSalud=historiaSalud
        self.tipoPelaje=tipoPelaje
        self.crearTablaCocker()
        
    
    def crearTablaCocker(self):
        """
        Se crea la tabla Cocker.
        """
        self.crearTabla("Cocker", "columnaCocker")
    
    def aggDatos(self, nombre, largoOrejas, nombreCriador, nivelEnergia, historiaSalud, tipoPelaje):
        """
        Agrega los datos a la base de datos.
        """
        datosCocker =[
            (f"{nombre}", f"{largoOrejas}", f"{nombreCriador}", f"{nivelEnergia}", f"{historiaSalud}", f"{tipoPelaje}")
        ]
        self.insertarDatos("Cocker", "?, ?, ?, ?, ?, ?", datosCocker)
        
    def getNombre(self):
        """
            Obtiene el nombre.
        """
        return self.nombre
        
    def setNombre(self, nuevoNombre):
        """
            Cambia el nombre por uno nuevo.
        """
        self.nombre=nuevoNombre
        
    def getLargoOrejas(self):
        """
            Obtiene el largo de las orejas.
        """
        return self.largoOrejas

    def setLargoOrejas(self, nuevoLargoOrejas):
        """
            Cambia el largo de las orejas por uno nuevo.
        """
        self.largoOrejas=nuevoLargoOrejas
    
    def getNombreCriador(self):
        """
            Obtiene el nombre del criador.
        """
        return self.nombreCriador
        
    def setNombreCriador(self, nuevoNombreCriador):
        """
            Cambia el nombre del criador por uno nuevo.
        """
        self.nombreCriador=nuevoNombreCriador
        
    def getNivelEnergia(self):
        """
            Obtiene el nivel de energía.
        """
        return self.nivelEnergia
        
    def setNivelEnergia(self, nuevoNivelEnergia):
        """
            Cambia el nivel de energia por uno nuevo.
        """
        self.nivelEnergia=nuevoNivelEnergia

    def getHistoriaSalud(self):
        """
            Obtiene la historia de salud.
        """
        return self.historiaSalud
        
    def setHistoriaSalud(self, nuevaHistoriaSalud):
        """
            Cambia la historia de salud por una nueva.
        """
        self.historiaSalud=nuevaHistoriaSalud

    def getTipoPelaje(self):
        """
            Obtiene el tipo de pelaje.
        """
        return self.tipoPelaje
        
    def setTipoPelaje(self, nuevoTipoPelaje):
        """
            Cambia el tipo de pelaje por uno nuevo.
        """
        self.tipoPelaje=nuevoTipoPelaje