import sqlite3
import json

class Conexion:
    def __init__(self):
        """
        Conectamos a la base de datos
        """
        self.miBaseDeDatos=sqlite3.connect("Entregable_2/BaseDeDatos.sqlite")
        self.pointer=self.miBaseDeDatos.cursor()

        with open("Entregable_2/Query.json", "r") as Documento:
            self.Queries=json.load(Documento)

    def crearTabla(self, nombreTabla="", columnas=""):
        """
        Creamos la tabla de la base de datos
        """
        if nombreTabla!="" and columnas!="":
            info=self.Queries["crearTabla"].format(nombreTabla, self.Queries[columnas])
            self.pointer.execute(info)
            self.miBaseDeDatos.commit()
            print("Mi base de datos: la tabla ", nombreTabla, " se ha creado correctamente.")
            
    def insertarDatos(self, tabla="", interrogantes="", valores=""):
        """
        Insertamos los datos a la tabla
        """
        if tabla!="" and valores!="":
            info=self.Queries["insertarDatos"].format(tabla, interrogantes)
            self.pointer.executemany(info, valores)
            self.miBaseDeDatos.commit()
            print("Mi base de datos: Los datos se agregaron correctamente en ", tabla)
    
    def leerDatos(self, tabla="", interrogantes="", valores=""):
        """
        Lee los datos de la base de datos
        """
        if tabla!="" and valores!="":
            info=self.Queries["leerDatos"].format(tabla, interrogantes)
            self.pointer.executemany(info, valores)
            self.miBaseDeDatos.commit()
            print(info)

    def eliminarDatos(self, tabla="", interrogantes="", valores=""):
        """
        Elimina un dato de la base de datos
        """
        if tabla!="" and valores!="":
            info=self.Queries["eliminarDatos"].format(tabla, interrogantes)
            self.pointer.execute(info, valores)
            self.miBaseDeDatos.commit()
            print("Dato eliminado")
        
    def actualizarDatos(self, tabla="", interrogantes="", valores=""):
        """
        Actualiza un dato de la base de datos
        """
        if tabla!="" and valores!="":
            info=self.Queries["actualizarDatos"].format(tabla, interrogantes)
            self.pointer.executemany(info,valores)
            self.miBaseDeDatos.commit()
            print("Dato actualizado")
