import pandas as pd

class Excel:
    def __init__(self):
        self.xlsx="Entregable_2/Clases.xlsx"

    def leerHoja(self, hoja=""):
        if hoja!="":
            self.hoja=pd.read_excel(self.xlsx, sheet_name=hoja)
            return self.hoja