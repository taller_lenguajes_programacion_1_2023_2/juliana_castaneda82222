from Perro import Perro
from Cocker import Cocker

crearTablaP=Perro()
crearTablaP.crearTablaPerro()
crearTablaP.aggDatos("Golden Retriever", "Dorado", "Largo", "Juguetona", "Grande", "Larga")
crearTablaP.aggDatos("Doberman", "Café", "Corto", "Inteligente", "Grande", "Larga")
crearTablaP.aggDatos("Shih Tzu", "Blanco con café", "Largo", "Alegre", "Pequeño", "Mediana")
crearTablaC=Cocker()
crearTablaC.crearTablaCocker()
crearTablaC.aggDatos("Wolden", "Largas", "Juliana", "Alto", "Excelente", "Liso")
crearTablaC.aggDatos("Niño", "Largas", "Monica", "Medio", "Vejez", "Crespo")
crearTablaC.aggDatos("Toby", "Largas", "Sebastian", "Bajo", "Falta Vacuna", "Ondulado")

