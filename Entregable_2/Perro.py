import pandas as pd
from Excel import Excel
from Conexion import Conexion

class Perro(Conexion):
    def __init__(self, raza="", color="", pelaje="", personalidad="", tamanio="", cola=""):
        super().__init__()
        """
        Atributos(Todos son de tipo String):
        Raza: Qué tipo de raza es el perro
        Color: De qué color es el perro
        Pelaje: Longitud del pelaje(Largo, mediano o corto)
        Personalidad: Qué tipo de personalidad tiene(alegre, amistoso, etc)
        Tamaño: De qué tamaño es el perro(Grande, mediano, pequeño o miniatura)
        Cola: De qué tamaño tiene la cola(Larga, mediana o corta)

        """
        self.raza=raza
        self.color=color
        self.pelaje=pelaje
        self.personalidad=personalidad
        self.tamanio=tamanio
        self.cola=cola
            
    def crearTablaPerro(self):
        """
        Crea la tabla Perro
        """
        self.crearTabla("Perro", "columnaPerro")
    
    def aggDatos(self, raza, color, pelaje, personalidad, tamanio, cola):
        """
        Agrega los datos a la tabla Perro
        """
        datosPerro =[
            (f"{raza}", f"{color}", f"{pelaje}", f"{personalidad}", f"{tamanio}", f"{cola}")
        ]
        self.insertarDatos("Perro", "?, ?, ?, ?, ?, ?", datosPerro)

    def getRaza(self):
        """
            Obtiene la raza.
        """
        return self.raza
         
    def setRaza(self, nuevaRaza):
        """
            Cambia la raza por una nueva.
        """
        self.raza=nuevaRaza

    def getColor(self):
        """
            Obtiene el color.
        """
        return self.color

    def setColor(self, nuevoColor):
        """
            Cambia el color por uno nuevo.
        """
        self.color=nuevoColor

    def getPelaje(self):
        """
            Obtiene el pelaje.
        """
        return self.pelaje

    def setPelaje(self, nuevoPelaje):
        """
            Cambia el pelaje por uno nuevo.
        """
        self.pelaje=nuevoPelaje

    def getPersonalidad(self):
        """
            Obtiene la personalidad.
        """
        return self.personalidad

    def setPersonalidad(self, nuevaPersonalidad):
        """
            Cambia la personalidad por una nueva.
        """
        self.personalidad=nuevaPersonalidad

    def getTamanio(self):
        """
            Obtiene el tamaño.
        """
        return self.tamanio
        
    def setTamanio(self, nuevoTamanio):
        """
            Cambia el tamaño por uno nuevo.
        """
        self.tamanio=nuevoTamanio

    def getCola(self):
        """
            Obtiene la cola.
        """
        return self.cola
        
    def setCola(self, nuevaCola):
        """
            Cambia la cola por una nueva.
        """
        self.cola=nuevaCola