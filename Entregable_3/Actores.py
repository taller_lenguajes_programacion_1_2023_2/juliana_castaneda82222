from Conexion import Conexion

class Actores(Conexion):
    def __init__(self, nombre="", fechaNacimiento=""):
        Conexion.__init__(self)
        """
        Atributos:
        nombre(str): Nombre del actor.
        fechaNacimiento(str): Fecha de nacimiento del actor.
        crearTabla(funcion): creará la tabla de la base de datos.
        """
        self.nombre = nombre
        self.fechaNacimiento = fechaNacimiento
        self.crearTabla("Actores", "Actores")

    def aggDatos(self):
        """
        Función para agregar los datos a la base de datos.
        """
        Datos=[
            (f"{self.nombre}", f"{self.fechaNacimiento}")
        ]
        self.insertarDato("Actores", 2, Datos)

    def verTabla(self):
        """
        Función para ver la tabla de la base de datos.
        """
        self.datosEnLaTabla = self.seleccionarTabla("Actores")