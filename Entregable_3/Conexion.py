import sqlite3
import json

class Conexion:
    def __init__(self):
        """
        Conectamos la base de datos.
        """
        self.baseDeDatos = sqlite3.connect("./Entregable_3/BaseDeDatos.sqlite")
        self.pointer = self.baseDeDatos.cursor()
        with open("./Entregable_3/Queries.json", "r") as queries:
            self.query = json.load(queries)

    def crearTabla(self, nombre, columnas):
        """
        Función para crear la tabla en la base de datos.
        """
        queryCrearTabla = self.query["CrearTabla"].format(nombre, self.query[columnas])
        self.pointer.execute(queryCrearTabla)
        self.baseDeDatos.commit()

    def insertarDato(self, tabla, columnas, datos):
        """
        Función para insertar los datos en la base de datos.
        """
        queryInsertarDatos = self.query["InsertarDatos"].format(tabla,', '.join(['?'] * columnas))
        self.pointer.executemany(queryInsertarDatos,datos)
        self.baseDeDatos.commit()

    def seleccionarTabla(self, tabla):
        """
        Función para selecionar en la tabla de la base de datos.
        """
        querySeleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.pointer.execute(querySeleccionarTabla)
        contenido = self.pointer.fetchall()
        return contenido
    
    def actualizarDatos(self, tabla, columna, nuevovalor, id):
        """
        Función para actualizar los datos en la base de datos.
        """
        queryActualizarDatos = self.query["ActualizarDatos"].format(tabla,columna,nuevovalor,'ID',id)
        self.pointer.execute(queryActualizarDatos)
        self.baseDeDatos.commit()

    def eliminarDatos(self, tabla, id):
        """
        Función para eliminar datos en la base de datos.
        """
        queryEliminarDatos = self.query["EliminarDatos"].format(tabla,'ID',id)
        self.pointer.execute(queryEliminarDatos)
        self.baseDeDatos.commit()