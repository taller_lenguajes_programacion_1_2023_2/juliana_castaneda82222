import tkinter as tk
from tkinter import messagebox, ttk
from Actores import Actores

class InterfazActores:
    def __init__(self):
        """
        Se crea la ventana principal.
        Se crea los botones superiores.
        Se crea los botones laterales.
        """
        self.ventanaPrincipal = tk.Tk()
        self.frameDatosActores = tk.Frame(self.ventanaPrincipal)
        self.ventanaPrincipal.title("Base de datos Actores")
        self.ventanaPrincipal.resizable(False, False)
        self.frameDatosActores = tk.Frame(self.ventanaPrincipal)
        self.frameDatosActores.grid(row=0, column=0, padx=5, pady=5)
        self.frameBotonesSuperior = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesSuperior.grid(row=1, column=0, padx=5, pady=5)
        self.frameBaseDeDatos = tk.Frame(self.ventanaPrincipal)
        self.frameBaseDeDatos.grid(row=2, column=0, padx=5, pady=5)
        self.frameBotonesLateral = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesLateral.grid(row=2,column=1, padx=5, pady=5)
        self.actores = Actores()
        self.camposDeTexto()
        self.botonesSuperiores()
        self.crearTabla()
        self.vincularBaseDeDatos()
        self.botonesLateral()
        self.ventanaPrincipal.mainloop()
    
    def crearTabla(self):
        """
        Se crea la tabla en la interfaz gráfica.
        """
        self.tablaBaseDeDatos = ttk.Treeview(self.frameBaseDeDatos,show="headings")
        self.tablaBaseDeDatos.config(columns=("ID", "Nombre del actor", "Fecha de Nacimiento"))
        self.tablaBaseDeDatos.heading("ID", text="ID")
        self.tablaBaseDeDatos.heading("Nombre del actor", text="NOMBRE")
        self.tablaBaseDeDatos.heading("Fecha de Nacimiento", text="FECHA_DE_NACIMIENTO")
        self.tablaBaseDeDatos.grid(row=0, column=0)

    def camposDeTexto(self):
        """
        Se crean los cuadros de texto 'Nombre', 'FechaNacimiento'.
        """
        self.variableNombre = tk.StringVar()
        self.textoNombre = tk.Label(self.frameDatosActores, text="Nombre del actor: ")
        self.textoNombre.grid(row=0, column=0)
        self.cuadroNombre = tk.Entry(self.frameDatosActores, textvariable=self.variableNombre)
        self.cuadroNombre.grid(row=0, column=1)
        
        self.variableFechaNacimiento = tk.StringVar()
        self.textoFechaNacimiento = tk.Label(self.frameDatosActores, text="Fecha de Nacimiento: ")
        self.textoFechaNacimiento.grid(row=1, column=0)
        self.cuadroFechaNacimiento = tk.Entry(self.frameDatosActores, textvariable=self.variableFechaNacimiento)
        self.cuadroFechaNacimiento.grid(row=1, column=1)
        
    def botonesSuperiores(self):
        """
        Se crean los botones superiores: Nuevo, Guardar, Cancelar.
        """
        self.botonNuevo = tk.Button(self.frameBotonesSuperior,text="Nuevo", bg="yellow", command=self.funcionNuevo)
        self.botonNuevo.grid(row=0, column=0, padx=5)
        
        self.botonGuardar = tk.Button(self.frameBotonesSuperior,text="Guardar", bg="yellow", command=self.funcionGuardar)
        self.botonGuardar.grid(row=0, column=1, padx=5)
        
        self.botonCancelar = tk.Button(self.frameBotonesSuperior,text="Cancelar", bg="yellow", command=self.funcionCancelar)
        self.botonCancelar.grid(row=0, column=2, padx=5)

    def funcionNuevo(self):
        """
        Se le da funcionalidad al botón 'Nuevo'.
        """
        self.variableNombre.set('')
        self.variableFechaNacimiento.set('')       
        self.cuadroNombre.config(state='normal')
        self.cuadroFechaNacimiento.config(state='normal')    
        self.botonGuardar.config(state='normal')
        self.botonCancelar.config(state='normal')

    def funcionCancelar(self):
        """
        Se le da funcionalidad al botón 'Cancelar'.
        """
        self.variableNombre.set('')
        self.variableFechaNacimiento.set('')      
        self.cuadroNombre.config(state='disabled')
        self.cuadroFechaNacimiento.config(state='disabled')     
        self.botonGuardar.config(state='disabled')
        self.botonCancelar.config(state='disabled')  

    def funcionGuardar(self):
        """
        Se le da funcionalidad al botón 'Guardar'.
        """
        self.actores.nombre = self.variableNombre.get()
        self.actores.fechaNacimiento = self.variableFechaNacimiento.get()
        messagebox.showinfo("Base de datos Actores","Se guardaron los datos en la tabla.")
        self.actores.aggDatos()
        self.vincularBaseDeDatos()

    def vincularBaseDeDatos(self):
        """
        Vinculamos la interfaz con la base de datos.
        """
        self.actores.verTabla()
        self.tablaBaseDeDatos.delete(*self.tablaBaseDeDatos.get_children())
        for fila in self.actores.datosEnLaTabla:
            self.tablaBaseDeDatos.insert("","end",values=fila)

    def botonesLateral(self):
        """
        Se crean los botones laterales: Editar, Eliminar.
        Se crea un cuadro de texto 'ID'
        """
        self.variableID = tk.IntVar()
        self.textoID = tk.Label(self.frameBotonesLateral, text="ID: ")
        self.textoID.grid(row=0, column=0)
        self.cuadroID = tk.Entry(self.frameBotonesLateral, textvariable=self.variableID)
        self.cuadroID.grid(row=1, column=0)        
        
        self.botonEditar = tk.Button(self.frameBotonesLateral,text="Editar", bg="yellow", command=self.funcionEditar)
        self.botonEditar.grid(row=2, column=0, pady=5)
        
        self.botonEliminar = tk.Button(self.frameBotonesLateral,text="Eliminar", bg="yellow", command=self.funcionEliminar)
        self.botonEliminar.grid(row=3, column=0, pady=5) 

    def funcionEditar(self):
        """
        Se le da funcionalidad al botón 'Editar'.
        """
        if self.variableNombre.get():
            self.actores.actualizarDatos("Actores","NOMBRE",self.variableNombre.get(),self.variableID.get()) 
            messagebox.showinfo("Base de datos Actores","Se editaron los datos en la tabla (Nombre).")  
            
        if self.variableFechaNacimiento.get():
            self.actores.actualizarDatos("Actores","FECHA_DE_NACIMIENTO",self.variableFechaNacimiento.get(),self.variableID.get())  
            messagebox.showinfo("Base de datos Actores","Se editaron los datos en la tabla (Fecha de Nacimiento).")
             
        self.vincularBaseDeDatos()
        
    def funcionEliminar(self):
        """
        Se le da funcionalidad al botón 'Eliminar'.
        """
        self.actores.eliminarDatos("Actores",self.variableID.get())
        messagebox.showinfo("Base de datos Actores","Se eliminaron los datos en la tabla.")
        self.vincularBaseDeDatos()
        
aplicacion = InterfazActores()