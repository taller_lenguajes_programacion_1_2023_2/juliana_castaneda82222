import tkinter as tk
from tkinter import messagebox, ttk
from Peliculas import Peliculas

class InterfazPeliculas:
    def __init__(self):
        """
        Se crea la ventana principal.
        Se crea los botones superiores.
        Se crea los botones laterales.
        """
        self.ventanaPrincipal = tk.Tk()
        self.frameDatosPeliculas = tk.Frame(self.ventanaPrincipal)
        self.ventanaPrincipal.title("Base de datos Películas")
        self.ventanaPrincipal.resizable(False, False)
        self.frameDatosPeliculas = tk.Frame(self.ventanaPrincipal)
        self.frameDatosPeliculas.grid(row=0, column=0, padx=5, pady=5)
        self.frameBotonesSuperior = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesSuperior.grid(row=1, column=0, padx=5, pady=5)
        self.frameBaseDeDatos = tk.Frame(self.ventanaPrincipal)
        self.frameBaseDeDatos.grid(row=2, column=0, padx=5, pady=5)
        self.frameBotonesLateral = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesLateral.grid(row=2,column=1, padx=5, pady=5)
        self.peliculas = Peliculas()
        self.camposDeTexto()
        self.botonesSuperiores()
        self.crearTabla()
        self.vincularBaseDeDatos()
        self.botonesLateral()
        self.ventanaPrincipal.mainloop()
    
    def crearTabla(self):
        """
        Se crea la tabla en la interfaz gráfica.
        """
        self.tablaBaseDeDatos = ttk.Treeview(self.frameBaseDeDatos,show="headings")
        self.tablaBaseDeDatos.config(columns=("ID","Titulo de la pelicula","ActorID"))
        self.tablaBaseDeDatos.heading("ID", text="ID")
        self.tablaBaseDeDatos.heading("Titulo de la pelicula", text="TITULO")
        self.tablaBaseDeDatos.heading("ActorID", text="ActorID")
        self.tablaBaseDeDatos.grid(row=0, column=0)

    def camposDeTexto(self):
        """
        Se crean los cuadros de texto 'Título'.
        """
        self.variableTitulo = tk.StringVar()
        self.textoTitulo = tk.Label(self.frameDatosPeliculas, text="Titulo de la pelicula: ")
        self.textoTitulo.grid(row=0, column=0)
        self.cuadroTitulo = tk.Entry(self.frameDatosPeliculas, textvariable=self.variableTitulo)
        self.cuadroTitulo.grid(row=0, column=1)
        
    def botonesSuperiores(self):
        """
        Se crean los botones superiores: Nuevo, Guardar, Cancelar.
        """
        self.botonNuevo = tk.Button(self.frameBotonesSuperior,text="Nuevo", bg="red", command=self.funcionNuevo)
        self.botonNuevo.grid(row=0, column=0, padx=5)
        
        self.botonGuardar = tk.Button(self.frameBotonesSuperior,text="Guardar", bg="red", command=self.funcionGuardar)
        self.botonGuardar.grid(row=0, column=1, padx=5)
        
        self.botonCancelar = tk.Button(self.frameBotonesSuperior,text="Cancelar", bg="red", command=self.funcionCancelar)
        self.botonCancelar.grid(row=0, column=2, padx=5)

    def funcionNuevo(self):
        """
        Se le da funcionalidad al botón 'Nuevo'.
        """
        self.variableTitulo.set('')       
        self.cuadroTitulo.config(state='normal')    
        self.botonGuardar.config(state='normal')
        self.botonCancelar.config(state='normal')

    def funcionCancelar(self):
        """
        Se le da funcionalidad al botón 'Cancelar'.
        """
        self.variableTitulo.set('')      
        self.cuadroTitulo.config(state='disabled')     
        self.botonGuardar.config(state='disabled')
        self.botonCancelar.config(state='disabled')  

    def funcionGuardar(self):
        """
        Se le da funcionalidad al botón 'Guardar'.
        """
        self.peliculas.titulo = self.variableTitulo.get()
        messagebox.showinfo("Base de datos Actores","Se guardaron los datos en la tabla.")
        self.peliculas.aggDatos()
        self.vincularBaseDeDatos()

    def vincularBaseDeDatos(self):
        """
        Vinculamos la interfaz con la base de datos.
        """
        self.peliculas.verTabla()
        self.tablaBaseDeDatos.delete(*self.tablaBaseDeDatos.get_children())
        for fila in self.peliculas.datosEnLaTabla:
            self.tablaBaseDeDatos.insert("","end",values=fila)

    def botonesLateral(self):
        """
        Se crean los botones laterales: Editar, Eliminar.
        Se crea un cuadro de texto 'ID'
        """
        self.variableID = tk.IntVar()
        self.textoID = tk.Label(self.frameBotonesLateral, text="ID: ")
        self.textoID.grid(row=0, column=0)
        self.cuadroID = tk.Entry(self.frameBotonesLateral, textvariable=self.variableID)
        self.cuadroID.grid(row=1, column=0)        
        
        self.botonEditar = tk.Button(self.frameBotonesLateral,text="Editar", bg="red", command=self.funcionEditar)
        self.botonEditar.grid(row=2, column=0, pady=5)
        
        self.botonEliminar = tk.Button(self.frameBotonesLateral,text="Eliminar", bg="red", command=self.funcionEliminar)
        self.botonEliminar.grid(row=3, column=0, pady=5) 

    def funcionEditar(self):
        """
        Se le da funcionalidad al botón 'Editar'.
        """
        if self.variableTitulo.get():
            self.peliculas.actualizarDatos("Peliculas","TITULO",self.variableTitulo.get(),self.variableID.get()) 
            messagebox.showinfo("Base de datos Películas","Se editaron los datos en la tabla (Título).")  
             
        self.vincularBaseDeDatos()
        
    def funcionEliminar(self):
        """
        Se le da funcionalidad al botón 'Eliminar'.
        """
        self.peliculas.eliminarDatos("Peliculas",self.variableID.get())
        messagebox.showinfo("Base de datos Películas","Se eliminaron los datos en la tabla.")
        self.vincularBaseDeDatos()
        
aplicacion = InterfazPeliculas()