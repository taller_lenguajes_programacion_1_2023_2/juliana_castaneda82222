from Conexion import Conexion

class Peliculas(Conexion):
    def __init__(self, titulo="", actorID=0):
        Conexion.__init__(self)
        """
        Atributos:
        titulo(str): Título de la película.
        actorID(int): El ID del actor.

        crearTabla(función): crea la tabla de la base de datos.
        """
        self.titulo = titulo
        self.actorID = actorID
        self.crearTabla("Peliculas", "Peliculas")

    def aggDatos(self):
        "Función para agregar los datos en la base de datos."
        Datos=[
            (f"{self.titulo}", f"{self.actorID}")
        ]
        self.insertarDato("Peliculas", 2, Datos)

    def verTabla(self):
        """
        Función para ver la tabla de la base de datos.
        """
        self.datosEnLaTabla = self.seleccionarTabla("Peliculas")