from django.urls import path
from .views import paginaInicio, paginaRegistro, paginaLogin, paginaMarketplace, paginaCategoria, paginaProducto, paginaCategoria2

urlpatterns = [
    path('', paginaInicio),
    path('Registro/', paginaRegistro),
    path('Login/', paginaLogin),
    path('Marketplace/', paginaMarketplace),
    path('Categoria/', paginaCategoria),
    path('Categoria2/', paginaCategoria2),
    path('Producto/<str:parametro1>/', paginaProducto, name="Producto"),
]