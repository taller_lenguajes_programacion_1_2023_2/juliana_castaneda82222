from Conexion import Conexion

class Cliente(Conexion):
    def __init__(self, id=0, nombre="", apellido="", dni="", telefono="", direccion="", correoElectronico=""):
        super().__init__()
        self.id=id
        self.nombre=nombre
        self.apellido=apellido
        self.dni=dni
        self.telefono=telefono
        self.direccion=direccion
        self.correoElectronico=correoElectronico

    def crearTabla(self):
        self.createTabla("Cliente", "columnaCliente")

    def aggDatos(self, nombre, apellido, dni, telefono, direccion, correoElectronico):
        DatosCliente=[
            (f"{nombre}", f"{apellido}", f"{dni}", f"{telefono}", f"{direccion}", f"{correoElectronico}")
        ]
        self.insertDatos("Cliente", "?, ?, ?, ?, ?, ?", DatosCliente)

    def getId(self):
        return self.id
    
    def setId(self, nuevoId):
        self.id=nuevoId

    def getNombre(self):
        return self.nombre
    
    def setNombre(self, nuevoNombre):
        self.nombre=nuevoNombre

    def getApellido(self):
        return self.apellido
    
    def setApellido(self, nuevoApellido):
        self.apellido=nuevoApellido
    
    def getDni(self):
        return self.dni
    
    def setDni(self, nuevoDni):
        self.dni=nuevoDni

    def getTelefono(self):
        return self.telefono
    
    def setTelefono(self, nuevoTelefono):
        self.telefono=nuevoTelefono

    def getDireccion(self):
        return self.direccion
    
    def setDireccion(self, nuevaDireccion):
        self.direccion=nuevaDireccion

    def getCorreoElectronico(self):
        return self.correoElectronico
    
    def setCorreoElectronico(self, nuevoCorreoElectronico):
        self.correoElectronico=nuevoCorreoElectronico

    def __str__(self):
        return f"Cliente(Id: {self.id}\nNombre: {self.nombre}\nApellido: {self.apellido}\nDni: {self.dni}\nTelefono: {self.telefono}\nDireccion: {self.direccion}\nCorreo Electrónico: {self.correoElectronico})"
    