import sqlite3
import json

class Conexion:
    def __init__(self):
        self.miBaseDeDatos=sqlite3.connect("Parcial_1/BaseDatos.sqlite")
        self.pointer=self.miBaseDeDatos.cursor()

        with open("Parcial_1/Query.json", "r") as Documento:
            self.Queries=json.load(Documento)

    def createTabla(self, nombreTabla="", columnas=""):
        if nombreTabla!="" and columnas!="":
            info=self.Queries["crearTabla"].format(nombreTabla, self.Queries[columnas])
            self.pointer.execute(info)
            self.miBaseDeDatos.commit()
            print("Mi base de datos: la tabla ", nombreTabla, " se ha creado correctamente.")
            
    def insertDatos(self, tabla="", interrogantes="", valores=""):
        if tabla!="" and valores!="":
            info=self.Queries["insertarDatos"].format(tabla, interrogantes)
            self.pointer.executemany(info, valores)
            self.miBaseDeDatos.commit()
            print("Mi base de datos: Los datos se agregaron correctamente en ", tabla)
    
    def selectDatos(self, tabla="", id=""):
        if tabla!="" and id!="":
            info=self.Queries["leerDatos"].format(tabla, id)
            self.pointer.executemany(info)
            self.miBaseDeDatos.commit()
            return self.pointer.fetchone()

    def deleteDatos(self, tabla="", id=""):
        if tabla!="" and id!="":
            info=self.Queries["eliminarDatos"].format(tabla, id)
            self.pointer.execute(info)
            self.miBaseDeDatos.commit()
            print("Dato eliminado")
        
    def actualizarDatos(self, tabla="", new="", dato="", id=""):
        if tabla!="" and dato!="" and new!="":
            info=self.Queries["eliminarDatos"].format(tabla, dato, new, id)
            self.pointer.execute(info)
            self.miBaseDeDatos.commit()
            print("Dato actualizado")