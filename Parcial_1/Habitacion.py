from Conexion import Conexion

class Habitacion(Conexion):
    def __init__(self, id=0, numero=0, tipo="", capacidad=0, precioNoche=0.0, amenities="", vista=""):
        super().__init__()
        self.id=id
        self.numero=numero
        self.tipo=tipo
        self.capacidad=capacidad
        self.precioNoche=precioNoche
        self.amenities=amenities
        self.vista=vista

    def crearTabla(self):
        self.createTabla("Habitacion", "columnaHabitacion")

    def aggDatos(self, numero, tipo, capacidad, precioNoche, amenities, vista):
        Datos=[
            (f"{numero}", f"{tipo}", f"{capacidad}", f"{precioNoche}", f"{amenities}", f"{vista}")
        ]
        self.insertDatos("Habitacion", "?, ?, ?, ?, ?, ?", Datos)

    def getId(self):
        return self.id
    
    def setId(self, nuevoId):
        self.id=nuevoId

    def getNumero(self):
        return self.numero
    
    def setNumero(self, nuevoNumero):
        self.numero=nuevoNumero

    def getTipo(self):
        return self.tipo
    
    def setTipo(self, nuevoTipo):
        self.tipo=nuevoTipo
    
    def getCapacidad(self):
        return self.capacidad
    
    def setCapacidad(self, nuevaCapacidad):
        self.capacidad=nuevaCapacidad

    def getPrecioNoche(self):
        return self.precioNoche
    
    def setPrecioNoche(self, nuevoPrecioNoche):
        self.precioNoche=nuevoPrecioNoche

    def getAmenities(self):
        return self.amenities
    
    def setAmenities(self, nuevoAmenities):
        self.amenities=nuevoAmenities

    def getVista(self):
        return self.vista
    
    def setVista(self, nuevaVista):
        self.vista=nuevaVista

    def __str__(self):
        return f"Habitacion(Id: {self.id}\nNumero: {self.numero}\nTipo: {self.tipo}\nCapacidad: {self.capacidad}\nPrecio Noche: {self.precioNoche}\nAmenities: {self.amenities}\nVista: {self.vista}"
    