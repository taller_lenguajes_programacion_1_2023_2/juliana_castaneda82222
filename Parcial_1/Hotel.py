from Conexion import Conexion

class Hotel(Conexion):
    def __init__(self, id=0, nombre="", direccion="", estrellas=0, servicios="", numeroHabitaciones=0, dueño=""):
        super().__init__()
        self.nombre=nombre
        self.direccion=direccion
        self.estrellas=estrellas
        self.servicios=servicios
        self.numeroHabitaciones=numeroHabitaciones
        self.dueño=dueño
        self.id=id
    
    def crearTabla(self):
        self.createTabla("Hotel", "columnaHotel")

    def aggDatos(self, nombre, direccion, estrellas, servicios, numeroHabitaciones, dueño):
        DatosHotel=[
            (f"{nombre}", f"{direccion}", f"{estrellas}", f"{servicios}", f"{numeroHabitaciones}", f"{dueño}")
        ]
        self.insertDatos("Hotel", "?, ?, ?, ?, ?, ?", DatosHotel)


    def getId(self):
        return self.id
    
    def setId(self, nuevoId):
        self.id=nuevoId
        
    def getNombre(self):
        return self.nombre
    
    def setNombre(self, nuevoNombre):
        self.nombre=nuevoNombre

    def getDireccion(self):
        return self.direccion
    
    def setDireccion(self, nuevaDireccion):
        self.direccion=nuevaDireccion
    
    def getEstrellas(self):
        return self.estrellas
    
    def setEstrellas(self, nuevaEstrella):
        self.estrellas=nuevaEstrella

    def getServicios(self):
        return self.servicios
    
    def setServicios(self, nuevoServicio):
        self.servicios=nuevoServicio

    def getNumeroHabitaciones(self):
        return self.numeroHabitaciones
    
    def setNumeroHabitaciones(self, nuevoNumeroHabitaciones):
        self.numeroHabitaciones=nuevoNumeroHabitaciones

    def getDueño(self):
        return self.dueño
    
    def setDueño(self, nuevoDueño):
        self.dueño=nuevoDueño

    def __str__(self):
        return f"Hotel(Id: {self.id}\nNombre: {self.nombre}\nDireccion: {self.direccion}\nEstrellas: {self.estrellas}\nServicios: {self.servicios}\nNumero de habitaciones: {self.numeroHabitaciones}\nDueño: {self.dueño})"
    