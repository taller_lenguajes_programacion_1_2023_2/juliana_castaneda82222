from Habitacion import Habitacion
from Cliente import Cliente
from Hotel import Hotel
from Conexion import Conexion

class Reserva(Habitacion, Conexion):
    def __init__(self, cliente=Cliente(), hotel=Hotel(), id="", numero=0, tipo="", capacidad=0, precioNoche=0.0, amenities="", vista="", fechaEntrada="", fechaSalida="", total=0.0):
        super().__init__(id, numero, tipo, capacidad, precioNoche, amenities, vista)
        self.id=id
        self.fechaEntrada=fechaEntrada
        self.fechaSalida=fechaSalida
        self.total=total
        self.cliente=cliente
        self.hotel=hotel

    def crearTabla(self):
        self.createTabla("Reserva", "columnaReserva")

    def aggDatos(self, fechaEntrada, fechaSalida, total, cliente, hotel):
        Datos=[
            (f"{fechaEntrada}", f"{fechaSalida}", f"{total}", f"{cliente}", f"{hotel}")
        ]
        self.insertDatos("Reserva", "?, ?, ?, ?, ?", Datos)

    def getFechaEntrada(self):
        return self.fechaEntrada
    
    def setFechaEntrada(self, nuevaFechaEntrada):
        self.fechaEntrada=nuevaFechaEntrada

    def getFechaSalida(self):
        return self.fechaSalida
    
    def setFechaSalida(self, nuevaFechaSalida):
        self.fechaSalida=nuevaFechaSalida

    def __str__(self):
        return f"Reserva(Fecha Entrada: {self.fechaEntrada}\nFecha Salida: {self.fechaSalida}\nTotal: {self.total}\nCliente: {self.cliente}\nHotel: {self.hotel})"
    