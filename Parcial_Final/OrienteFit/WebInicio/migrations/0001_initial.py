# Generated by Django 4.2.7 on 2023-11-16 07:24

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Personas',
            fields=[
                ('ID', models.IntegerField(primary_key=True, serialize=False)),
                ('NOMBRE', models.TextField(max_length=30)),
                ('APELLIDO', models.TextField(max_length=30)),
                ('TELEFONO', models.TextField(max_length=30)),
                ('DIRECCION', models.TextField(max_length=50)),
                ('EMAIL', models.EmailField(max_length=30)),
                ('USUARIO', models.TextField(max_length=30)),
                ('CLAVE', models.TextField(max_length=30)),
            ],
        ),
    ]
