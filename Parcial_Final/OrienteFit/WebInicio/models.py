from django.db import models

class Personas(models.Model):
    ID = models.IntegerField(primary_key=True)
    NOMBRE = models.TextField(max_length=30)
    APELLIDO = models.TextField(max_length=30)
    TELEFONO = models.TextField(max_length=30)
    DIRECCION = models.TextField(max_length=50)
    EMAIL = models.EmailField(max_length=30)
    USUARIO = models.TextField(max_length=30)
    CLAVE = models.TextField(max_length=30)

