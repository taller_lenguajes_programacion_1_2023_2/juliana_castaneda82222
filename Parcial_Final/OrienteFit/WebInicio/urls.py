from django.urls import path
from .views import paginaInicio, paginaRegistro, paginaLogin, paginaTienda, paginaProducto

urlpatterns = [
    path('', paginaInicio),
    path('Registro/', paginaRegistro),
    path('Login/', paginaLogin),
    path('Tienda/', paginaTienda),
    path('Producto/<str:parametro1>/', paginaProducto, name="Producto"),
]