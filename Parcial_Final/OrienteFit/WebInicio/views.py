from django.shortcuts import render, redirect
from .models import Personas
import time

def paginaInicio(request):
    return render(request, 'inicio.html')

def paginaRegistro(request):
    error = ""

    if request.method == 'POST':
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        telefono = request.POST['telefono']
        direccion = request.POST['direccion']
        email = request.POST['email']
        usuario = request.POST['usuario']
        clave = request.POST['clave']
        if Personas.objects.filter(EMAIL=email).exists():
            error = "*El usuario ya está registrado."
        elif Personas.objects.filter(USUARIO=usuario).exists():
            error = "*Este nombre de usuario ya está ocupado."
        else:
            nuevoDato = Personas.objects.create(
                NOMBRE=nombre,
                APELLIDO=apellido,
                TELEFONO=telefono,
                DIRECCION=direccion,
                EMAIL=email,
                USUARIO=usuario,
                CLAVE=clave
            )
            return redirect('/Login/')

    return render(request, 'registro.html', {"error": error})

def paginaLogin(request):
    error = ""
    if request.method == 'POST':
        try:
            usuario = request.POST['usuario']
            clave = request.POST['clave']
            verificarUsuario = Personas.objects.get(USUARIO=usuario)
            if verificarUsuario.CLAVE == clave:
                time.sleep(3)
                return redirect('/')
            else:
                error = "*La contraseña ingresada es incorrecta."
        except:
            error = "*El usuario no está registrado."
            
    return render(request, 'login.html', {
        "error":error
    })

def paginaTienda(request):
    return render(request, 'tienda.html')

def paginaProducto(request, parametro1):
    productos = [
        {"nombre": "Nitro tech", "descripcion": "Es pura y simple, y cumple con sus promesas. Una sola cucharada medidora de Nitro-Tech® contiene 30 g de proteína de suero de leche de primera calidad que aporta aminoácidos esenciales y aminoácidos de cadena ramificada. Se filtra con la tecnología de filtración multifase para eliminar las grasas y los carbohidratos no deseados. Después de todo, el contenido es lo que cuenta.", "precio": "$ 200.000,00", "imagen": "/static/img/nitrotech.png", "info": "/static/img/infoNitro.jpg"},
        {"nombre": "ISO100", "descripcion": "ISO 100 de 1.4 lbs de vainilla gourmet de la marca Dymatize es un suplemento de proteína de suero de alta calidad diseñado para satisfacer las necesidades de los atletas y entusiastas del fitness. Cada porción de 25 gramos de este producto ofrece 20 servicios en total y proporciona 25 gramos de proteína. ISO 100 de Dymatize utiliza una fórmula de proteína de suero aislada que ha sido procesada para eliminar la mayoría de los carbohidratos y las grasas, brindando una proteína de rápida absorción y altamente biodisponible.Esto significa que tu cuerpo puede utilizar eficientemente los aminoácidos esenciales presentes en el suero de leche para la síntesis de proteínas y la recuperación muscular.", "precio": "$ 380.000,00", "imagen": "/static/img/proteina1.png", "info": "/static/img/isoInfo.webp"},
        {"nombre": "Gold standard whey", "descripcion": "En más de 90 países, durante más de 30 años, se ha confiado en Optimum Nutrition® para obtener la más alta calidad en recuperación posentrenamiento, energía preentrenamiento y nutrición deportiva para llevar. Después de una cuidadosa selección de proveedores, cada ingrediente se prueba para asegurar una pureza, potencia y composición excepcionales. Cumplimos con los más altos estándares de producción para que pueda liberar todo el potencial del cuerpo.", "precio": "$ 250.000,00", "imagen": "/static/img/proteina2.png", "info": "/static/img/infoGold.png"},
        {"nombre": "Omega", "descripcion": "Las grasas son esenciales para nuestro buen funcionamiento y son las grasas poliinsaturadas, las cuales son omega 3 y omega 6. Estos 2 ácidos grasos deberían estar en equilibrio, ya que en exceso el omega 6, puede llegar a ser INFLAMATORIO; por el contrario un aporte adecuado de omega 3, será ANTIINFLAMATORIO. Y en múltiples estudios omega 3 ha demostrado su beneficio al disminuir el riesgo cardiovascular e incluso se utilizan como terapia coadyuvante para disminuir trigliceridos.", "precio": "$ 70.000,00", "imagen": "/static/img/omega.webp", "info": "/static/img/infoOmega.png"},
        {"nombre": "Whey", "descripcion": "Whey Pure de 5 libras es un suplemento de proteína en polvo de alta calidad, formulado con aislado de proteína de suero de leche. Cada envase contiene 30 porciones, con 26 gramos de proteína por porción. El aislado de proteína de suero de leche es una de las formas más puras de proteína de suero de leche, lo que significa que contiene una mayor proporción de proteína y menos grasa y lactosa en comparación con otros tipos de proteína de suero de leche. Esto lo hace ideal para aquellos que buscan aumentar su ingesta de proteínas mientras mantienen bajo el consumo de carbohidratos y grasas.", "precio": "$ 280.000,00", "imagen": "/static/img/whey.jpg", "info": "/static/img/infoWhey.webp"},
        {"nombre": "C4", "descripcion": "Favorece la quema de masa grasa y la atención durante los entrenamientos. Es pura energía gracias a la tecnología NO3 y a ingredientes exclusivos, como la Creatina Nitrato. C4 es la fórmula pre-entreno más poderosa de su clase. Posee la capacidad de incendiar tu mente, músculos y entrenamiento día tras día. ¡Con C4 obtendrás más energía y fuerza de lo que jamás habrías podido imaginar. Obtendrás esa repetición extra, completar la última serie… arrasando tus marcas anteriores tanto de fuerza como de resistencia!", "precio": "$ 170.000,00", "imagen": "/static/img/pre1.png", "info": "/static/img/infoC4.webp"},
        {"nombre": "Legacy", "descripcion": "Es nuestra creatina HCL de mejor y mas rapida absorcion, por lo que siempre vas a requerir menos cantidad para obtener los mismos resultados; es un producto que reune tres reconocidas sustancias en el campo del deporte y el fitness: arginina, metionina y glicinaque tienen una gran biodisponibilidad y excelente absorcion y por ultimo el metabolito final de la leucina que garantiza la sintesis proteica y estilula el desarrolo de la masa muscular.", "precio": "$ 110.000,00", "imagen": "/static/img/legacy.png", "info": "/static/img/infoLegacy.webp"},
        {"nombre": "Creatine", "descripcion": "Una fórmula de construcción muscular de gran alcance que ofrece una efectiva dosis de 5 gramos de monohidrato de creatina de alta calidad, una de las más eficaces y validadas científicamente en el mundo para mejorar tu rendimiento. La creatina proporciona energía a las células musculares y promueve aumentos en fuerza y masa muscular.", "precio": "$ 95.000,00", "imagen": "/static/img/iron.jpeg", "info": "/static/img/infoIron.webp"},
        {"nombre": "CREASTACK", "descripcion": "Crea Stack es un producto diseñado para incrementar la fuerza, la resistencia y la energía en actividades físicas. Su fórmula contiene creatina monohidratada (la creatina con más evidencia científica), HMB (estimula la síntesis de proteínas musculares), ácido alfa lipoico (incrementa el transporte de energía a los músculos) y sulfato de vanadio (potencializa los efectos anabólicos de la insulina). El efecto combinado de sus ingredientes activan la energía muscular y estimulan la síntesis de proteínas musculares.", "precio": "$ 95.000,00", "imagen": "/static/img/creatina1.png", "info": "/static/img/infoCreastack.webp"},
        {"nombre": "CREATINE POWER", "descripcion": "Proteína suplementaria en apoyo de la masa muscular y la salud: Advantage Whey, Efectos de la suplementación de creatina de 4 semanas combinada con entrenamiento complejo sobre el daño muscular y el rendimiento deportivo, Efectos de la suplementación con creatina y tres días de entrenamiento de resistencia sobre la fuerza muscular, la producción de energía y la función neuromuscular", "precio": "$ 125.000,00", "imagen": "/static/img/creatina2.png", "info": "/static/img/infoCreatine.webp"},
        {"nombre": "Intenze", "descripcion": "Citrulina en 6 gramos, que permitirá a tu cuerpo aumentar los niveles de óxido nítrico, el cual genera vasodilatación para nutrir de manera adecuada el músculo; además 3 gramos de Arginina y el polvo de raíz de remolacha con su aporte de nitratos; ayudan a potenciar la acción de la citrulina, haciendo más efectiva esa vasodilatación.", "precio": "$ 135.000,00", "imagen": "/static/img/intenze.png", "info": "/static/img/infoIntenze.png"},
        {"nombre": "Myth", "descripcion": "MYTH es la primera bebida de su tipo que contiene: Extracto de naranja amarga y Glicerol, sustancias con numerosos beneficios para los deportistas y que por su propiedades físicas es de difícil manejo en productos en polvo.", "precio": "$ 120.000,00", "imagen": "/static/img/myth.jpeg", "info": "/static/img/infoMyth.webp"}
    ]

    producto_seleccionado = None
    for producto in productos:
        if producto["nombre"] == parametro1:
            producto_seleccionado = producto
            break

    

    return render(request, "producto.html", {"producto": producto_seleccionado})