# Taller de Programación 1

Bienvenidos al repositorio oficial del curso "Taller de Programación 1". Aquí encontrarán todos los recursos, fechas y temarios relacionados con el curso.

## Fechas de Evaluación

1. **Entregable 1**: 7 de Septiembre, 2023
2. **Entregable 2**: 28 de Septiembre, 2023
3. **Parcial 1**: 5 Octubre, 2023
4. **Entregable 3**: 2 Noviembre, 2023
5. **Entregable 4**: 30 Noviembre, 2023
6. **Parcial Final**: 7 Diciembre, 2023

## Herramientas Utilizadas

- **Git**: [Sitio oficial](https://git-scm.com/)
- **Visual Studio Code**: [Descargar aquí](https://code.visualstudio.com/) Extenciones (Python , Git grafh , Git lens)
- **Python 3.9.2**: [Python descarga](https://www.python.org/ftp/python/3.9.2/python-3.9.2-amd64.exe)

## Control de Versiones

- **git clone** <url>
- **git add** .  Empaquetar cambios
- **git commit -m "descripcion del cambio"** Etiquetar version
- **git push origin main** Carga o empuja el paquete a la ubicacion remota

## Espacio de Trabajo

- **python -m venv venv** creacion de espacio de trabajo
- **.\venv\Scripts\activate** activar entorno virtual

## Librerias

- **pip install <lib> (pandas, openpyxl)**
- **python.exe -m pip install --upgrade pip**
- **pip list**

## Comandos VSCode

- Control + ñ Terminal 

## Sesiones

| Sesión | Fecha       | Tema                                                      |
|--------|-------------|-----------------------------------------------------------|
| 1      | 09/08/2023  | Introducción a la programación                           |
| 2      | 10/08/2023  | Control de versiones                                     |
| 3      | 17/08/2023  | Lectura y escritura de archivos planos                   |
| 4      | 23/08/2023  | Agregar filas y columnas con pandas                      |