from Estudio import Estudio
from Habilidad import Habilidad

class Persona:
    def __init__(self,primernombre,primerapellido,segundoapellido,nacionalidad,telefono,email):
        self.primernombre=primernombre
        self.primerapellido=primerapellido
        self.segundoapellido=segundoapellido
        self.nacionalidad=nacionalidad
        self.telefono=telefono
        self.email=email       
        
    def __str__(self):
        return f"Primer nombre: {self.primernombre}\n"\
            f"Segundo nombre: {self.primerapellido}\n"\
                f"Apellido: {self.segundoapellido}\n"\
                    f"Nacionalidad: {self.nacionalidad}\n"\
                        f"Telefono: {self.telefono}\n"\
                            f"Email: {self.email}"
                            
    def setHabilidad(self, tipo, nivel, id):
        self.habilidad=Habilidad(tipo,nivel,id)
    def setEstudio(self, titulo, inicio, fin, horas):
        self.estudio=Estudio(titulo,inicio,fin,horas)
    def getHabilidad(self):
        print(self.habilidad)
    def getEstudio(self):
        print(self.estudio)
