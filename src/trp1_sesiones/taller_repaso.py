#Taller de repaso

#1. Imprimir Hola, Mundo!
print("Hola, Mundo!")


#2. Variables y asignación
variable = 0
print(variable)
variable += 1
print (variable)

#3.Tipos de datos básicos: int, float, string.
nombre = "Juliana" #str
print(type(nombre))

entero = 10 #int
print(type(entero))

flotante = 10.5 #float
print(type(flotante))

#4. Operaciones aritméticas básicas.
num1 = 58
num2 = 23

print(num1+num2)
print(num1-num2)
print(num1*num2)
print(num1%num2)

#5. Conversión entre tipos de datos.
n1 = 10
n2 = "20"

n3 = n1 + int(n2)
print(n3)

#6. Solicitar entrada del usuario.
print("Ingrese su nombre: ")
Nombre = input()
print(Nombre)

#7. Condicional if.
if(n3==30):
    print("La suma es correcta.")

#8. Condicional if-else.
variable=15
if(variable == 15):
    print("La variable es correcta")
else :
    print("Variable incorrecta")

#9. Condicional if-elif-else
x=4
y=2
if x>y:
    print("x es mayor que y")
elif x<y:
    print("y es mayor que x")
else:
    print("No se sabe cuál es mayor")

#10. Bucle for
for i in [1, 2, 3, 4]:
    print(i)

#11. Bucle while
i=0
while i<=3:
    print(i)
    i+=1

#12. Uso de break y continue.
num = 0
for num in range(10):
    if num == 5:
        break
    print("El numero es: "+str(num))

for num in range(10):
    if num == 5:
        continue
    print("El numero es: "+str(num))

#13. Listas y sus operaciones básicas.
lista = [1,2,3,4,5]
print(type(lista))

lista2 = lista.copy()
lista2.append(6)
print(lista2)

suma = 0
for num in lista:
    suma += num
print(suma)

#14. Tuplas y su inmutabilidad.
tupla = (10, 20, 30)

#15. Conjuntos y operaciones.
conjunto ={1, 2, 3}
conjunto.add(4)
conjunto.update([5,7,9,6])
conjunto.remove(3)
conjunto.clear()
len(conjunto)

#16. Diccionarios y operaciones clave-valor.
diccionario = {
     "Nombre": "Juliana",
     "Nombre": "Sebastian",
     "Edad": 24}
print(diccionario)

for x in diccionario:
    print(x)

for x in diccionario:
    print(diccionario[x])

#17. List funciones basicas.
lista = ['a', 'b', 'c', 'd']
print(len(lista))
lista.append('h')
lista.extend(['d','e','f'])


#18. Leer un archivo de texto.
with open(".\src\trp1_sesiones\Documentos\txt\sesion2.txt", "r") as archivo:
    print(archivo.read())

#19. Escribir en un archivo de texto.
with open(".\src\trp1_sesiones\Documentos\txt\sesion2.txt","w") as archivo:
    archivo.write("Escribiendo prueba")

#20. Modos de apertura: r, w, a, rb, wb.
with open(".\src\trp1_sesiones\Documentos\txt\sesion2.txt","a") as archivo: 
    archivo.write("Anexar")

with open(".\src\trp1_sesiones\Documentos\txt\sesion2.txt","wb") as archivo: 
    archivo.write(b"\x01\x02\x03")

with open(".\src\trp1_sesiones\Documentos\txt\sesion2.txt","rb") as archivo: 
    contenido=archivo.read()
    print(contenido)

#21. Trabajar con archivos JSON. Crear un Dataframe
print("\n21.")
import json

Datos={
    "Nombre":"Juliana",
    "Edad":"19",
    "Documento":"1033"    
}
with open(".\src\trp1_sesiones\Documentos\json\ejemplo.json","w") as js:
    json.dump(Datos, js)
with open(".\src\trp1_sesiones\Documentos\json\ejemplo.json","r") as js:
    Archivo=json.load(js)
print(Archivo)
    
# 22. Leer un archivo CSV
# 23. Filtrar datos en un DataFrame
print("\n22/23.")
import pandas as pd

df = pd.read_csv("src\trp1_sesiones\Documentos\txt\-sesion2.txt",sep=";") 
Filtrar=df[df["Fecha"]=="30/08/2023"]
print(Filtrar.to_string(index=False))

# 24. Operaciones básicas: sum(), mean(), max(), min()
print("\n24.")
import numpy as np

Numeros=[1,2,3,4,5]
print(f"Numeros: {Numeros}")
Sum=np.sum(Numeros)
print(f"Operacion sum: {Sum}")
Mean=np.mean(Numeros)
print(f"Operacion mean: {Mean}")
Max=np.max(Numeros)
print(f"Operacion max: {Max}")
Min=np.min(Numeros)
print(f"Operacion min: {Min}")

# 25. Uso de iloc y loc
print("\n25.")

df=pd.DataFrame({
  "Edad": [18, 25, 30, 40],
  "Genero": ["Hombre", "Hombre", "Mujer", "Mujer"]
})
print(f"Iloc:\n{df.iloc[0]}")
print("Loc:\n",df.loc[df["Edad"]==30])
print("Casilla:\n",df.loc[1]["Edad"])
print("Filtro:\n",df.loc[df["Edad"]==25,"Genero"])

# 26. Agrupar datos con groupby
print("\n26.")

df=pd.DataFrame({
    "nombre": ["Juan", "Pedro", "María", "José"],
    "edad": [20, 25, 30, 35]
})
df.groupby("nombre")
for name in df.groupby("nombre"):
    print(name)

# 27. Unir DataFrames con merge y concat
print("\n27.")

df1=pd.DataFrame({
    "A": [1, 2, 3],
    "B": [4, 5, 6]
})

df2=pd.DataFrame({
    "A": [1, 2, 3],
    "C": [7, 8, 9],
    "D": [10, 11, 12]
})

concat=pd.concat([df1, df2], axis=1)
print(f"Concat:\n{concat}")

merge=pd.merge(df1, df2, on="A")
print(f"Merge:\n{merge}")

# 28. Manipular series temporales
# Son datos que están ordenados cronológicamente
# Están asociados a un tiempo específico
print("\n28.")

Valores=(1,2,3,4,5)
series=pd.Series(Valores)

print(series)

# 29. Exportar un DataFrame a un CSV 
print("\n29.")

df=pd.DataFrame({
    "nombre": ["Juan", "Pedro", "María"],
    "edad": [20, 25, 30]
})
print(df)
df.to_csv(".\src\trp1_sesiones\Documentos\csv\Clase6.csv")

# 30. Convertir un DataFrame a JSON
print("\n30.")

df=pd.DataFrame({
    "nombre": ["Juan", "Pedro", "Maria"],
    "edad": [20, 25, 30]
})

df.to_json(".\src\trp1_sesiones\Documentos\csv\Clase6.csv")
print(df.to_json())
import pandas as pd
# 31. Lee una tabla HTML desde página
# 32.Guardar la tabla de html  en un excel
print("1/2.")
Tabla=pd.read_html("https://www.x-rates.com/table/?from=USD&amount=1")[1]
Tabla.to_excel("src\trp1_sesiones\Documentos\xlsx\Clase7v1.xlsx")
print("Guardado en Clase7v1.xlsx")

# 33. Exporta los primeros 10 a un archivo CSV
print("\n3.")
Tabla.head(10).to_csv(".\src\trp1_sesiones\Documentos\xlsx\Clase7v1.xlsx")
print("Guardado en Clase7v1.csv")

# 34. Lee tres tablas HTML y guárdalas en tres hojas
print("\n4.")
Hoja1=pd.read_html("https://help.netflix.com/es/node/24926")[0]
Hoja2=pd.read_html("https://www.x-rates.com/table/?from=USD&amount=1")[1]
Hoja3=pd.read_html("https://www.colombia.com/cambio-moneda/monedas-del-mundo/")[0]
Hoja2=Hoja2.tail(5)
Hoja3=Hoja3.head(5)
with pd.ExcelWriter(".\src\trp1_sesiones\Documentos\xlsx\Clase7v2.xlsx") as writer:
    Hoja1.to_excel(writer,sheet_name="Hoja1")
    Hoja2.to_excel(writer,sheet_name="Hoja2")
    Hoja3.to_excel(writer,sheet_name="Hoja3")
print("Guardado en Clase7v2.xlsx")

# 35. Filtra los registros donde la columna "edad" sea >=30
print("\n5.")
Documento=pd.read_excel(".\Sesiones\Sesion4\Clase4v1.xlsx")
Edades=Documento.loc[Documento["Edad"]>=30]
Edades.to_csv(".\Sesiones\Sesion7\Clase7v2.csv")
print("Guardado en Clase7v2.csv")

# 36. Lee un archivo CSV, guardar columnas "Nombre" y "Profesion"
print("\n6.")
Archivo=pd.read_csv(".\Sesiones\Sesion7\Clase7v3.csv")

Columnas = ['Nombre', 'Profesión']
Archivo[Columnas].to_excel(".\Sesiones\Sesion7\Clase7v3.xlsx")
print("Guardado en Clase7v3.xlsx")

# 37. Combinar dos hojas de un excel en un DataFrame
print("\n7.")
xlsx=pd.ExcelFile(".\Sesiones\Sesion7\Clase7v2.xlsx")
Hoja1=pd.read_excel(xlsx,"Hoja1")
Hoja2=pd.read_excel(xlsx,"Hoja2")
Combinar=pd.concat([Hoja1,Hoja2])
Combinar.to_excel(".\Sesiones\Sesion7\Clase7v4.xlsx")