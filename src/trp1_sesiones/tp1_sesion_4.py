#**********DATAFRAME PANDAS*********

#diccionario={
#    "Columna1":["PrimerDato",2],
#El encabezado es Columna1, El primer renglon en esa columna es PrimerRenglon,El segundo es 2
#    "Columna2":["SegundoDato",3]
#    El encabezado es Columna2, El primer renglon en esa columna es SegundoDato,El segundo es 3
#}

import pandas as pd

convertir=pd.read_excel("src/trp1_sesiones/Documentos/xlsx/clase_4_dataset.xlsx") # Guarda el DataFrame en una variable
# convertir.loc[Fila,"Fecha"]="23/08/2023" Agregar nueva columna al excel
# convertir.loc[len(convertir)]= ["Juliana",19,"Informatica",31051502,"23/08/2023"] Agregar nueva fila al excel
# nombre=convertir.loc[Fila,"Nombre"] Obtener el dato en esa casilla
# print(convertir.columns) Imprime el nombre de las columnas
# print(convertir.dtypes) Tipos de datos en columna (object para string, int64 para numeros)
# print(convertir.iloc[Fila]) Imprime una fila determinada
# print(convertir["Nombre"]) Imprime un columna determinada
# print(convertir["Nombre"].iloc[Fila]) Imprime el nombre en la fila especificada

for i in range(0,len(convertir)):
    convertir.loc[i,"Fecha"]="23/08/2023"
    
convertir.loc[len(convertir)]= ["Juliana",19,"Informatica",31051502,"23/08/2023"]
print(convertir)

convertir.to_excel("C:\Users\julia\OneDrive\Documentos\Taller_Programacion\juliana_castaneda82222\src\trp1_sesiones\Documentos\xlsx\clase_4_dataset.xlsx",index=False)

#for index,row in convertir.iterrows():
#    print("la fila {}, columna {}". format(index,row["Profesión"]))
# Imprime el indice de la fila, y lo que hay en la columna "Profesion" de esa fila

# | AGREGAR USANDO DICCIONARIO |
#datos = {
#    "Nombre": ["Andrés"],
#    "Edad": [21],
#    "Profesión": ["Informatica"],
#    "Teléfono": [3234108779],
#    "Fecha:" ["23/08/2023"]
#}
# convertir.loc[len(convertir)]=datos

# | AGREGAR USANDO LISTAS |
# datos=["Andres",21,"Informatica",3234108779,"23/08/2023"]
# convertir.loc[len(convertir)]=datos

# | DICCIONARIO A EXCEL |
#diccionario={
#    "Nombre":["Ricardo","Jhonki"],
#    "Documento":[1001757,1023485]
#}
#dataframe=pd.DataFrame.from_dict(diccionario)
#print(dataframe)

# | EXCEL A DICCIONARIO |
# diccionario=convertir.to_dict("records") Convierte el DataFrame a un diccionario
# for record in diccionario: Imprimir en el orden que se ve en Excel
#    print(record)

# | TIPS |
# producto = lambda a,b: a*b Crea una funcion con retorno en la definicion de variable
# print("Metodo lambda: ",producto(5,8))

# lista=[2,4,7,2,45,8]
# def area(r):
#    return 3.14*r*r
# lista2 = list(map(area,lista))  
# # El metodo map permite iterar una funcion con una lista, y guardarlo en otra lista
# lista2 = list(map(area,[i for i in range(20) if i%2==0])) Se pueden poner condicionales
# print(list(filter(lambda x:x>5,lista))) Funcion anonima

# lista = ["a","","jhf","","dhg","",""]
# print(list(filter(None,lista))) Excluye los Strings vacios

# lista=[i for i in range(1,11)] Crea una lista del 1 al 10
# lista=[diccionario["Nombre"], diccionario["Documento"]] Guardar en lista lo de un diccionario
# matriz=[[i,i**2] for i in range(1,11)] Crea una matriz de 10 filas y 2 columnas, guardandolo en [num,num²]

# matriz = [[1,4,7],[3,6,3],[9,7,5]] Matriz de 3 filas y 3 columnas
# matrizFlattened = [i for row in matriz for i in row] Convertir una matriz a un arreglo
